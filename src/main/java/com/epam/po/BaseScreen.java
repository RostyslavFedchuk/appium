package com.epam.po;

import com.epam.utils.AppiumDriverManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class BaseScreen {
    protected AppiumDriver<MobileElement> driver;

    public BaseScreen() {
        this.driver = AppiumDriverManager.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
}
