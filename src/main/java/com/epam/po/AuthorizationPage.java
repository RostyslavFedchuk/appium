package com.epam.po;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;

public class AuthorizationPage extends BaseScreen {
    @AndroidFindBy(id = "com.google.android.gm:id/welcome_tour_got_it")
    private MobileElement gotItButton;
    @AndroidFindBy(id = "com.google.android.gm:id/setup_addresses_add_another")
    private MobileElement addAnotherEmailButton;
    @AndroidFindBy(id = "com.google.android.gm:id/google_option")
    private MobileElement googleOption;
    @AndroidFindBy(id = "com.google.android.gm:id/suw_navbar_next")
    private MobileElement nextButton;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[3]/" +
            "android.view.View/android.view.View[1]/android.view.View[1]/android.widget.EditText")
    private MobileElement emailInput;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement submitEmailButton;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[3]/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.EditText")
    private MobileElement passwordInput;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement submitPasswordButton;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.webkit.WebView/android.view.View[1]" +
            "/android.view.View[6]/android.widget.Button")
    private MobileElement skipButton;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement agreeWithTermsButton;

    @Step("Pressing 'Got it' button step...")
    public void tapOnGotItButton(){
        gotItButton.click();
    }

    @Step("Adding another email step...")
    public void addAnotherEmail(){
        addAnotherEmailButton.click();
    }

    @Step("Choosing google option step...")
    public void setGoogleOption(){
        googleOption.click();
    }

    @Step("Pressing Next button step...")
    public void submitOption(){
        nextButton.click();
    }

    @Step("Filling Email field with email: {0} step...")
    public void fillEmail(String email) {
        emailInput.sendKeys(email);
    }

    @Step("Clicking SubmitEmail button step...")
    public void submitEmail() {
        submitEmailButton.click();
    }

    @Step("Filling Password field with password: {0} step...")
    public void fillPassword(String password) {
        passwordInput.sendKeys(password);
    }

    @Step("Clicking SubmitPassword button step...")
    public void submitPassword() {
        submitPasswordButton.click();
    }

    @Step("Skipping page about phone number step...")
    public void pressSkipButton(){
        skipButton.click();
    }

    @Step("Agreeing with terms step...")
    public void pressAgreeWithTermsButton(){
        agreeWithTermsButton.click();
    }
}
