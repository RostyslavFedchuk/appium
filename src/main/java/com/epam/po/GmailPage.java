package com.epam.po;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;

public class GmailPage extends BaseScreen {
    @AndroidFindBy(id = "com.google.android.gm:id/action_done")
    private MobileElement takeMeToGmailButton;
    @AndroidFindBy(id = "com.google.android.gm:id/compose_button")
    private MobileElement composeButton;
    @AndroidFindBy(id = "com.google.android.gm:id/to")
    private MobileElement receiverField;
    @AndroidFindBy(id = "com.google.android.gm:id/subject")
    private MobileElement subjectField;
    @AndroidFindBy(id = "com.google.android.gm:id/body")
    private MobileElement messageField;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='Send']")
    private MobileElement sendButton;
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
    private MobileElement menuButton;
    @AndroidFindBy(xpath = "//android.widget.ListView/android.widget.LinearLayout[8]")
    private MobileElement sentMessagesButton;
    @AndroidFindBy(xpath = "//android.widget.ListView/android.widget.FrameLayout[1]/android.view.View")
    private MobileElement lastSentMessage;
    @AndroidFindBy(id = "com.google.android.gm:id/subject_and_folder_view")
    private MobileElement conversationHeader;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"Delete\"]")
    private MobileElement deleteMessageButton;
    @AndroidFindBy(id = "com.google.android.gm:id/action_text")
    private MobileElement undoDeletingButton;

    @Step("Taping 'Take me tp Gmail' button")
    public void tapTakeMeToGmailButton(){
        takeMeToGmailButton.click();
    }

    @Step("Taping Compose button step...")
    public void tapComposeButton() {
        composeButton.click();
    }

    @Step("Filling Receiver field with receiver: {0} step...")
    public void fillReceiverField(String receiver) {
        receiverField.sendKeys(receiver);
    }

    @Step("Filling Subject field with subject: {0} step...")
    public void fillSubjectField(String subject) {
        subjectField.sendKeys(subject);
    }

    @Step("Filling Message field with message: {0} step...")
    public void fillMessageField(String message) {
        messageField.sendKeys(message);
    }

    @Step("Taping Send button step...")
    public void tapSendButton() {
        sendButton.click();
    }

    @Step("Taping Menu button step...")
    public void tapMenuButton() {
        menuButton.click();
    }

    @Step("Taping Menu button step...")
    public void tapSentMessagesButton() {
        sentMessagesButton.click();
    }

    @Step("Taping on the last sent message step...")
    public void tapOnLastSentMessage() {
        lastSentMessage.click();
    }

    @Step("Taking subject from the last Sent Message step...")
    public String getSubjectFormLastSentMessage(){
        return conversationHeader.getText();
    }

    @Step("Taping DeleteMessage button step...")
    public void tapDeleteMessage() {
        deleteMessageButton.click();
    }

    @Step("Trying to find UndoDeletingButton step...")
    public MobileElement getUndoDeletingButton() {
        return undoDeletingButton;
    }
}
