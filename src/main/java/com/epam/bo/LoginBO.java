package com.epam.bo;

import com.epam.model.User;
import com.epam.po.AuthorizationPage;
import com.epam.utils.AppiumDriverManager;

public class LoginBO {
    private AuthorizationPage authorizationPage;

    public LoginBO() {
        authorizationPage = new AuthorizationPage();
    }

    public void login(User user) {
        authorizationPage.tapOnGotItButton();
        authorizationPage.addAnotherEmail();
        authorizationPage.setGoogleOption();
        authorizationPage.submitOption();
        authorizationPage.fillEmail(user.getLogin());
        authorizationPage.submitEmail();
        authorizationPage.fillPassword(user.getPassword());
        authorizationPage.submitPassword();
        //authorizationPage.pressSkipButton();
        authorizationPage.pressAgreeWithTermsButton();
    }
}
