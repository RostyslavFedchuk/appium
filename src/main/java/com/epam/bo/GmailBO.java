package com.epam.bo;

import com.epam.model.Email;
import com.epam.po.GmailPage;
import com.epam.utils.AppiumDriverManager;

public class GmailBO {
    private GmailPage gmailPage;

    public GmailBO() {
        gmailPage = new GmailPage();
        gmailPage.tapTakeMeToGmailButton();
    }

    public void sendMessage(Email email) {
        gmailPage.tapComposeButton();
        gmailPage.fillReceiverField(email.getReceiver());
        gmailPage.fillSubjectField(email.getSubject());
        gmailPage.fillMessageField(email.getMessage());
        gmailPage.tapSendButton();
    }

    public boolean deleteJustWrittenMessage(Email email) {
        gmailPage.tapMenuButton();
        gmailPage.tapSentMessagesButton();
        gmailPage.tapOnLastSentMessage();
        if(gmailPage.getSubjectFormLastSentMessage().toLowerCase()
                .contains(email.getSubject().toLowerCase())){
            gmailPage.tapDeleteMessage();
            return true;
        }
        return false;
    }

    public boolean isUndoDeletingButtonDisplayed() {
        return gmailPage.getUndoDeletingButton().isDisplayed();
    }
}
