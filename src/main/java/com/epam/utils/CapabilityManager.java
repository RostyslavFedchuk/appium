package com.epam.utils;

import org.openqa.selenium.remote.DesiredCapabilities;

public class CapabilityManager {
    private static DesiredCapabilities capabilities;

    public static DesiredCapabilities getCapabilities(){
        if (capabilities == null){
            setCaps();
        }
        return capabilities;
    }

    private static void setCaps(){
        capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "Pixel XL");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "7.0");
        capabilities.setCapability("skipUnlock","true");
        capabilities.setCapability("appPackage", "com.google.android.gm");
        capabilities.setCapability("appActivity","com.google.android.gm.ConversationListActivityGmail");
        capabilities.setCapability("noReset","false");
    }
}
