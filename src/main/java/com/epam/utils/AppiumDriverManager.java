package com.epam.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import java.net.URL;

public class AppiumDriverManager {
    private static AppiumDriver<MobileElement> appiumDriver;

    public static AppiumDriver<MobileElement> getDriver(){
        try {
            if(appiumDriver == null) {
                appiumDriver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub")
                        , CapabilityManager.getCapabilities());
            }
            appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return appiumDriver;
    }

    public static void quit(){
        appiumDriver.quit();
        appiumDriver = null;
    }
}
