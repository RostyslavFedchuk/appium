package com.epam.bo;

import com.epam.utils.AppiumDriverManager;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {
    private static final Logger LOGGER = LogManager.getLogger(ITestListener.class);

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenShotsPNG(AppiumDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    private static String getTestMethodName(ITestResult result){
        return result.getMethod().getConstructorOrMethod().getName();
    }

    @Override
    public void onTestFailure(ITestResult result){
        LOGGER.info("Test Failure!!! Screenshot captured for test case: "
                + getTestMethodName(result));
        saveScreenShotsPNG(AppiumDriverManager.getDriver());
    }
}
