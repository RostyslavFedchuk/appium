package com.epam.bo;

import com.epam.model.Email;
import com.epam.model.User;
import com.epam.utils.AppiumDriverManager;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

import static org.testng.Assert.assertTrue;

@Listeners({TestListener.class})
public class SendingAndDeletingEmailTest {
    private static ResourceBundle bundle = ResourceBundle.getBundle("config");

    private Email getEmail() {
        return new Email(bundle.getString("EMAIL_RECEIVER"),
                "Hello my dear friend!", "SeleniumTest");
    }

    private User getUser() {
        return new User("zzhak.EvKalipt@gmail.com","love_T0Rtuk");
    }

    @Test
    public void testSendAndDeleteEmail() {
//        LoginBO loginBO = new LoginBO();
//        loginBO.login(getUser());
        GmailBO gmail = new GmailBO();
        gmail.sendMessage(getEmail());
        assertTrue(gmail.deleteJustWrittenMessage(getEmail()));
        assertTrue(gmail.isUndoDeletingButtonDisplayed());
    }

    @AfterMethod
    public void quit() {
        AppiumDriverManager.quit();
    }
}